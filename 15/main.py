import numpy as np
import itertools
import os
import time
import math

entrylist = [[0,3,6],[3,2,1],[2,3,1],[1,2,3],[2,1,3],[1,3,2]]
start = time.time()
iteration = 0
for entry in entrylist:
    while len(entry)<2020:
        iteration += 1
        if entry[-1] not in entry[:-1]:
            entry.append(0)
        else:
            for i in np.arange(len(entry)-2,-1,-1):
                if entry[i] == entry[-1]:
                    entry.append(len(entry) - (i+1))
                    break
    print(entry[-1])


##################### PART 2
entry = [16,12,1,0,15,7,11]
pos = {}

print("creating big dictionary")

for i in range(30000000):
    pos[i] = -1
print("created")

for i in entry:
    pos[i] = entry.index(i) + 1

while len(entry)<30000000:
    if len(entry) % 300000 == 0:
        print(time.time()-start,len(entry))
    if pos[entry[-1]] == -1:
        pos[entry[-1]] = len(entry)
        entry.append(0)
    else:
        entry.append(len(entry) - pos[entry[-1]])
        pos[entry[-2]] = len(entry)-1


print(entry[-1])






"""
while len(entry)<30000000:
    if len(entry) % 3000 == 0:
        print(time.time()-start,len(entry))
    #iteration += 1
    #if entry[-1] not in entry[:-1]:
    #    entry.append(0)
    #else:
    #    for i in np.arange(len(entry)-2,-1,-1):
    #        if entry[i] == entry[-1]:
    #            entry.append(len(entry) - (i+1))
    #            break
    if entry[-1] in entry[:-1]:    
        entry.append(0)
    else:
        entry.append(len(entry) - (entry.index(entry[-1])+1))
        entry[entry.index(entry[-1])] = -1"""
