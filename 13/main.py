import numpy as np
import itertools
import os
import time
import math

liste = []
with open('entry.dat') as entry:
    for l in entry:
        if l != '\n':
            liste.append(l[:-1])

timestamp = int(liste[0])
busliste = liste[1].split(",")

print(busliste)
Min = np.inf
busid = 0
for i in busliste:
    if i != "x":
        if ((timestamp//int(i)) + 1) *int(i) - timestamp < Min and ((timestamp//int(i)) + 1) * int(i)-timestamp > 0 :
            print((timestamp//int(i) + 1) *int(i) - timestamp)
            Min = (timestamp//int(i) + 1) *int(i) - timestamp
            busid = int(i)


print(Min* busid )


#################################### PART 2

def solve_congruence(a, b, m):
    if b == 0:
        return 0

    if a < 0:
        a = -a
        b = -b

    b %= m
    while a > m:
        a -= m

    return (m * solve_congruence(m, -b, a) + b) // a



def solve_linear_congruence(a, b, m):
    """ Describe all solutions to ax = b  (mod m), or raise ValueError. """
    g = math.gcd(a, m)
    if b % g:
        raise ValueError("No solutions")
    a, b, m = a//g, b//g, m//g
    return pow(a, -1, m) * b % m




def solve_congruence_system(sys_array):
    
    B = sys_array[0][1]
    C = sys_array[0][2]
    j = 1
    print(len(sys_array))
    ##### x = b mod(c) ----> x = c*j + b
    for i in sys_array[1:]:
        print(j)
        D = i[1]
        E = i[2]

        ###### x = d mod(e) ----> c*j + b = d mod(e)
        res = solve_linear_congruence(C,D-B,E)
        ######## j = e * k + res

        B = C*res + B
        C = E*C
        j+=1

        
        print(B,C)
    return B,C







    
congr_sys = []

for i in busliste:
    if i != "x":
        congr_sys.append([1,-busliste.index(i),i])

print(congr_sys)
temp = []
while len(congr_sys)!= 0:
    Max = -np.inf
    for i in congr_sys:
        if int(i[-1]) > Max:
            Max = int(i[-1])
            elem = i
    temp.append([int(elem[j]) for j in np.arange(len(elem))])
    congr_sys.remove(elem)

congr_sys = temp.copy()
print(congr_sys)

print(solve_congruence_system(congr_sys))
input()


