import numpy as np
import itertools
import os
import time
import math
import regex


entry_rule = {}
entry_rule_decoded = {}
messages = []
countiter = {}

with open('entry.dat') as entry:
    for l in entry:
        if ":" in l:
            entry_rule[l[:l.index(":")]] = l[l.index(":")+2:-1].replace('"','')
        elif l!="\n":
            messages.append(l[:-1])
length = []
for m in messages:
    if len(m) not in length:
        length.append(len(m))
print(length)
Max = max(length)
input()

def decode(numrule,rules):
    global entry_rule
    global entry_rule_decoded
    global countiter
    res = []
    print("I check ",numrule)

    if numrule in entry_rule_decoded.keys():
        print("I return for ",numrule)#,entry_rule_decoded[numrule])
        return entry_rule_decoded[numrule]
    
    elif entry_rule[numrule] in "ab":
        entry_rule_decoded[numrule] = [entry_rule[numrule]]
        print("I return for ",numrule)#,[entry_rule[numrule]])
        return [entry_rule[numrule]]

    else:
        rules_splited = rules.split("|")
        dec = {}

        for i in rules_splited:

            j = i.split(" ")
            #input()
           
            try:
                j.remove('')
            except:
                j=j
            
            args = []
            for k in np.arange(len(j)): 
                dec[k] = decode(j[k],entry_rule[j[k]])
                args.append(dec[k])

            print("I continue ",numrule)
            
            for combi in itertools.product(*args):
                sub_res = ""
                for sr in combi:
                    sub_res += sr
                res.append(sub_res)
                if numrule == "0" and len(res)%10000 == 0:
                    print("LEN : ",len(res))
        
        print("I return for ",numrule)
        entry_rule_decoded[numrule] = res
        return res
decode("0",entry_rule["0"])

count = 0
for i in entry_rule_decoded["0"]:
    if i in messages:
        count += 1
print(count)

################### PART 2

print(len(entry_rule_decoded["42"]))
print(len(entry_rule_decoded["31"]))


i = 0
j = 0
count = 0


for i in np.arange(10):
    for j in np.arange(10):
        suite = ["42"]
        for k in np.arange(i):
            suite.append("42")
        suite.append("42")
        for k in np.arange(j):
            suite.append("42")
        for k in np.arange(j):
            suite.append("31")
        suite.append("31")
        
        for m in messages:
            if len(m)%8 == 0 and len(m)/8 <= len(suite):
                finish = False
                for k in np.arange(int(len(m)/8)):

                    if m[8*k:8*(k+1)] not in entry_rule_decoded[suite[k]]:
                        break
                    elif k == len(suite) -1:
                        finish = True
                if finish:
                    count += 1
print(count)

