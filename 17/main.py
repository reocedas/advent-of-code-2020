import numpy as np
import itertools
import os
import time
import math


flat_ini = []

with open('entry.dat') as entry:
    for l in entry:
        if l!= '\n':
            flat_ini.append(l[0:-1])

print(flat_ini)
x_dim = len(flat_ini[0])+50
y_dim = len(flat_ini)+50
z_dim = 25
w_dim = 25

pok_dim = np.empty([x_dim,y_dim,z_dim],dtype = str)
middle = int(z_dim / 2) 

for x in np.arange(pok_dim.shape[0]):
    for y in np.arange(pok_dim.shape[1]):
        for z in np.arange(pok_dim.shape[2]):
            pok_dim[x,y,z] = "."

print(pok_dim.shape)
for i in flat_ini:
    for j in np.arange(len(i)):
        pok_dim[flat_ini.index(i)+int(x_dim/2),j+int(y_dim/2),middle] = i[j]


for i in np.arange(6):
    print(i)
    pok_dim_cop = np.copy(pok_dim)
    start = time.time()
    for x in np.arange(pok_dim.shape[0]):
        for y in np.arange(pok_dim.shape[1]):
            for z in np.arange(pok_dim.shape[2]):
                count = 0
                for x_ in [x-1,x,x+1]:
                    for y_ in [y-1,y,y+1]:
                        for z_ in [z-1,z,z+1]:
                            try:
                                if pok_dim[x_,y_,z_] == "#":
                                    count += 1
                            except:
                                edge = 1
                if pok_dim[x,y,z] == "#" and (count != 4 and count != 3):
                    pok_dim_cop[x,y,z] = "."
                elif pok_dim[x,y,z] == "." and count == 3:
                    pok_dim_cop[x,y,z] = "#"
    pok_dim = np.copy(pok_dim_cop)
count = 0
for x in np.arange(pok_dim.shape[0]):
    for y in np.arange(pok_dim.shape[1]):
        for z in np.arange(pok_dim.shape[2]):
            if pok_dim[x,y,z]=="#":
                count +=1
print(count)

######################################################## PART 2

pok_dim = np.empty([x_dim,y_dim,z_dim,w_dim],dtype = str)
middle_z = int(z_dim / 2) 
middle_w = int(w_dim / 2) 


for x in np.arange(pok_dim.shape[0]):
    for y in np.arange(pok_dim.shape[1]):
        for z in np.arange(pok_dim.shape[2]):
            for w in np.arange(pok_dim.shape[3]):
                pok_dim[x,y,z,w] = "."

print(pok_dim.shape)
for i in flat_ini:
    for j in np.arange(len(i)):
        pok_dim[flat_ini.index(i)+int(x_dim/2),j+int(y_dim/2),middle_z,middle_w] = i[j]


for i in np.arange(6):
    start = time.time()
    print(i)
    pok_dim_cop = np.copy(pok_dim)
    for x in np.arange(pok_dim.shape[0]):
        for y in np.arange(pok_dim.shape[1]):
            for z in np.arange(pok_dim.shape[2]):
                for w in np.arange(pok_dim.shape[3]):
                    count = 0
                    for x_ in [x-1,x,x+1]:
                        for y_ in [y-1,y,y+1]:
                            for z_ in [z-1,z,z+1]:
                                for w_ in [w-1,w,w+1]:
                                    try:
                                        if pok_dim[x_,y_,z_,w_] == "#":
                                            count += 1
                                    except:
                                        edge = 1
                    if pok_dim[x,y,z,w] == "#" and (count != 4 and count != 3):
                        pok_dim_cop[x,y,z,w] = "."
                    elif pok_dim[x,y,z,w] == "." and count == 3:
                        pok_dim_cop[x,y,z,w] = "#"
    pok_dim = np.copy(pok_dim_cop)
    print("executed in ",time.time()-start," s")
count = 0
for x in np.arange(pok_dim.shape[0]):
    for y in np.arange(pok_dim.shape[1]):
        for z in np.arange(pok_dim.shape[2]):
            for w in np.arange(pok_dim.shape[3]):
                if pok_dim[x,y,z,w]=="#":
                    count +=1
print(count)





