import numpy as np
import os

entry = open("entry.dat","r")

entry = entry.read().split('\n\n')

Passports = []
passport = []

for i in entry:
    Passports.append(i.split())

count = 0
value = []
fields = []
checkval = True

for i in Passports:
    for j in i:
        fields.append(j.split(":")[0])
        value.append(j.split(":")[1])
    if (('byr' in fields) and (int(value[fields.index("byr")]) >= 1920 and int(value[fields.index("byr")]) <= 2002) and
        ('iyr' in fields) and (int(value[fields.index("iyr")]) >= 2010 and int(value[fields.index("iyr")]) <= 2020) and
        ('eyr' in fields) and (int(value[fields.index("eyr")]) >= 2020 and int(value[fields.index("eyr")]) <= 2030) and
        ('hgt' in fields) and
        ('hcl' in fields) and
        ('ecl' in fields) and (value[fields.index("ecl")] in ['amb','blu','brn','gry','grn','hzl','oth']) and
        ('pid' in fields) and (len(value[fields.index("pid")]))==9):
        
        if "cm" in value[fields.index('hgt')]:
            
            if (int(value[fields.index('hgt')][0:-2]) >= 150 and int(value[fields.index('hgt')][0:-2]) <= 193) and ("#" in value[fields.index('hcl')] and len(value[fields.index('hcl')]) == 7):
                
                for k in value[fields.index('hcl')][1:]:
                    
                    if k not in '0123456789abcdef':
                        
                        checkval = False
                
                for k in value[fields.index("pid")]:
                    
                    if k not in '0123456789':
                        
                        checkval= False
            else:
                checkval = False
        
        elif "in" in value[fields.index('hgt')]:
            
            if (int(value[fields.index('hgt')][0:-2]) >= 59 and int(value[fields.index('hgt')][0:-2]) <= 76) and ("#" in value[fields.index('hcl')] and len(value[fields.index('hcl')]) == 7):
                
                for k in value[fields.index('hcl')][1:]:
                    
                    if k not in '0123456789abcdef':
                    
                        checkval = False
                
                for k in value[fields.index("pid")]:
                
                    if k not in '0123456789':
                    
                        checkval = False
            else:
                checkval = False
        else:
            checkval = False

                    
        if checkval:        
            print(fields[fields.index('hcl')])
            print(value[fields.index('hcl')],"#" in value[fields.index('hcl')])
            count +=1
    value = []
    fields=[]
    checkval = True

print(count)
#print(Passports[0])

