import numpy as np
import itertools
import os
import time

seats = []
with open('entry.dat') as entry:
    for l in entry:
        if l != '\n':
            seats.append([])
            for i in l:
                if i != '\n':
                    seats[-1].append(i)
seats = np.asarray(seats)
print(seats.shape)
stable = False
loop = 0
counts = np.zeros([seats.shape[0],seats.shape[1]])
while not stable:
    loop +=1
    if loop%10000 == 0:
        print(loop)
    seats_future = np.copy(seats)
    for i in np.arange(seats.shape[0]):
        for j in np.arange(seats.shape[1]):
            count = 0
            if i != 0 and j != 0 and i != seats.shape[0]-1 and j != seats.shape[1] - 1:###### center case
                for k in [i+1,i,i-1]:
                    for l in [j+1,j,j-1]:
                        if seats[k,l] == "#":
                            count += 1
                #print(i,j)
            elif j != 0 and i != seats.shape[0]-1 and j != seats.shape[1] - 1:###### i=0 and rest
                for k in [i+1,i]:
                    for l in [j+1,j,j-1]:
                        if seats[k,l] == "#":
                            count += 1
                #print(i,j)
            elif i != 0 and i != seats.shape[0]-1 and j != seats.shape[1] - 1: ###### j=0 and rest
                for k in [i+1,i,i-1]:
                    for l in [j+1,j]:
                        if seats[k,l] == "#":
                            count += 1
                #print(i,j)
            elif i != seats.shape[0]-1 and j != seats.shape[1] - 1:###### i=j=0
                for k in [i+1,i]:
                    for l in [j+1,j]:
                        if seats[k,l] == "#":
                            count += 1
                #print(i,j)
            elif i!= 0 and j != 0 and j != seats.shape[1] - 1:###### i = lim
                for k in [i-1,i]:
                    for l in [j+1,j,j-1]:
                        if seats[k,l] == "#":
                            count += 1
                #print(i,j)
            elif i!= 0 and j != 0 and i != seats.shape[0] - 1:###### j = lim
                for k in [i-1,i,i+1]:
                    for l in [j,j-1]:
                        if seats[k,l] == "#":
                            count += 1
                #print(i,j)
            elif i!= 0 and j != 0:###### i=j=lim
                for k in [i-1,i]:
                    for l in [j,j-1]:
                        if seats[k,l] == "#":
                            count += 1
                #print(i,j)
            elif i!= 0 and j != seats.shape[1] - 1:######i=lim j=0
                for k in [i-1,i]:
                    for l in [j+1,j]:
                        if seats[k,l] == "#":
                            count += 1
                #print(i,j)
            elif j!= 0 and i != seats.shape[0] - 1:######j=lim i=0
                for k in [i+1,i]:
                    for l in [j-1,j]:
                        if seats[k,l] == "#":
                            count += 1
                #print(i,j)
            counts[i,j] = count
            if count >= 5 and seats[i,j] == "#":
                seats_future[i,j] = "L"
            elif count == 0 and seats[i,j] == "L":
                seats_future[i,j] = "#"
    if (seats_future == seats).all():
        stable = True
        break
    else :
        #for i in np.arange(seats.shape[0]):
        #    print(seats[i],"----->",counts[i],"----->",seats_future[i])
        #input()
        seats = np.copy(seats_future)
finish = 0
for i in np.arange(seats.shape[0]):
    for j in np.arange(seats.shape[1]):
        if seats[i,j] == "#":
            finish +=1
print(finish)

############################################################################ PART 2

seats = []
with open('entry.dat') as entry:
    for l in entry:
        if l != '\n':
            seats.append([])
            for i in l:
                if i != '\n':
                    seats[-1].append(i)
seats = np.asarray(seats)
counts = np.zeros([seats.shape[0],seats.shape[1]])

directions = [[-1,-1],[0,-1],[-1,1],[0,1],[1,-1],[1,0],[1,1],[-1,0]]

stable = False
while not stable:
    seats_future = np.copy(seats)
    for i in np.arange(seats.shape[0]):
        for j in np.arange(seats.shape[1]):
            count = 0
            for enum_dir in directions:
                m = 0
                while True:
                    m+=1
                    k = enum_dir[0]*m
                    l = enum_dir[1]*m
                    if i+k >= seats.shape[0] or i+k < 0 or j+l >= seats.shape[1] or j+l < 0:
                        break
                    elif seats[i+k,j+l] == "#":
                        count += 1
                        break
                    elif seats[i+k,j+l] == "L":
                        break
                   
                #print(i,j)
            counts[i,j] = count
            if count >= 5 and seats[i,j] == "#":
                seats_future[i,j] = "L"
            elif count == 0 and seats[i,j] == "L":
                seats_future[i,j] = "#"
    if (seats_future == seats).all():
        stable = True
        break
    else :
        #for i in np.arange(seats.shape[-1]):
        #    print(seats[i],"->",counts[i],"->",seats_future[i])
        #input()
        seats = np.copy(seats_future)
finish = 0
for i in np.arange(seats.shape[0]):
    for j in np.arange(seats.shape[1]):
        if seats[i,j] == "#":
            finish +=1
print(finish)


