import numpy as np
import itertools
import os
import time
import math

def apply_mask(BinVal,Mask):
    while len(BinVal) != len(Mask):
        BinVal = "0"+BinVal
    BinVal = list(BinVal) 
    for i in np.arange(len(Mask)):
        if Mask[i] != "X":
            BinVal[i] = Mask[i]
    return "".join(BinVal)



liste = []
with open('entry.dat') as entry:
    for l in entry:
        if l != '\n':
            liste.append(l[:-1])




memory = {}
for i in liste:
    if "mask" in i:
        mask = i[7:]
    else:
        mem_addr = int(i[4:i.index(']')])
        val = int(i[i.index(' = ')+3:])
        memory[mem_addr] = int(apply_mask(bin(val)[2:],mask),2)
#print(memory)
tot = 0
for i in memory:
    tot += memory[i]

print(tot)

########################### PART 2

def apply_mask_V2(BinVal,Mask):
    while len(BinVal) != len(Mask):
        BinVal = "0"+BinVal
    BinVal = list(BinVal) 
    indexes = []
    for i in np.arange(len(Mask)):
        if Mask[i] != "0":
            BinVal[i] = Mask[i]
            if Mask[i] == "X":
                indexes.append(i)
                BinVal[i] = Mask[i]

    alt =  list(itertools.product(range(2),repeat = len(indexes)))
    combi = []
    for i in alt:
        temp = BinVal.copy()
        for j in indexes:
            temp[j] = str(i[indexes.index(j)])
        combi.append(int("".join(temp),2))

    return combi

memory = {}
for i in liste:
    if "mask" in i:
        mask = i[7:]
    else:
        mem_addr = bin(int(i[4:i.index(']')]))[2:]
        mem_addr = apply_mask_V2(mem_addr,mask)

        val = int(i[i.index(' = ')+3:])
        for i in mem_addr:
            memory[i] = val 

print(memory)

tot = 0

for i in memory:
    tot += memory[i]

print(tot)






















