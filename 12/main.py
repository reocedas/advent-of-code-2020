import numpy as np
import itertools
import os
import time

instr = []
with open('entry.dat') as entry:
    for l in entry:
        if l != '\n':
            instr.append(l[:-1])

card = ["e","s","w","n"]
dist = {"e":0,"n":0,"w":0,"s":0}

face = "e"
for i in instr:
    if i[0] == "R":
        if int(i[1:]) != 90:
            print("R:",int(i[1:]),face,"--->",card[(card.index(face) + int(int(i[1:])/90))%len(card)])
        face = card[(card.index(face) + int(int(i[1:])/90))%len(card)]
    elif i[0] == "L":
        if int(i[1:]) != 90:
            print("L:",int(i[1:]),face,"--->",card[(card.index(face) + int(int(i[1:])/90))%len(card)])
        face = card[(card.index(face) - int(int(i[1:])/90))%len(card)]
    elif i[0] == "F":
        dist[face] += int(i[1:])
    else:
        dist[i[0].lower()] += int(i[1:])


print(np.abs(dist["e"] - dist["w"]) + np.abs(dist["n"] - dist["s"]))


############################################################################### PART 2



dist = {"e":0,"n":0,"w":0,"s":0}
waypoint = {"e":10,"n":1,"w":0,"s":0}
waybuff = {}
for i in instr:
    #print("wp: ",waypoint)
    #print("dist:",dist)
    #print(i)
    #input()
    if i[0] == "R":
        num_turn = int(int(i[1:]))/90
        for j in np.arange(num_turn):
            for k in np.arange(len(card)):
                waybuff[card[(k+1)%len(card)]]=waypoint[card[k]]
            waypoint = waybuff.copy()

    elif i[0] == "L":
        num_turn = int(int(i[1:]))/90
        for j in np.arange(num_turn):
            for k in np.arange(len(card)):
                waybuff[card[(k-1)%len(card)]]=waypoint[card[k]]
            waypoint = waybuff.copy()

    elif i[0] == "F":
        for j in dist:
            dist[j] += waypoint[j] * int(i[1:])
    else:
        waypoint[i[0].lower()] += int(i[1:])


print(np.abs(dist["e"] - dist["w"]) + np.abs(dist["n"] - dist["s"]))

print(dist)
