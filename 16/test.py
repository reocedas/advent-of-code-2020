import time

import math
memory = []


def solve_congruence(a, b, m):
    
    
    if b == 0:
        return 0

    if a < 0:
        a = -a
        b = -b

    b %= m
    while a > m:
        a -= m
    
    return (m * solve_congruence(m, -b, a) + b) // a

def solve_linear_congruence(a, b, m):
    """ Describe all solutions to ax = b  (mod m), or raise ValueError. """
    g = math.gcd(a, m)
    if b % g:
        raise ValueError("No solutions")
    a, b, m = a//g, b//g, m//g
    return pow(a, -1, m) * b % m



start = time.time()
A = [[1,6,7],[1,4,5],[1,1,3]]
print("1 : ",solve_linear_congruence(1,6,7)) 
print("2 : ",solve_congruence(1,6,7)) 
print(time.time()-start)
def solve_congruence_system(sys_array):
    
    B = sys_array[0][1]
    C = sys_array[0][2]

    ##### x = b mod(c) ----> x = c*j + b
    for i in sys_array[1:]:
        D = i[1]
        E = i[2]

        ###### x = d mod(e) ----> c*j + b = d mod(e)
        res = solve_linear_congruence(C,D-B,E)
        ######## j = e * k + res

        B = C*res + B
        C = E*C

        

    return B,C

print(solve_congruence_system(A))
