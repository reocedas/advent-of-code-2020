import numpy as np
import itertools
import os
import time
import math

fieldsterm = False

fields = {}
totval = []
tickets = []


with open('entry.dat') as entry:
    for l in entry:
        if l!= '\n':
            if not fieldsterm:
                temp = l[l.index(': ')+2:].split(' ')
                fields[l[:l.index(': ')]] = np.append(np.arange(int(temp[0].split('-')[0]),int(temp[0].split('-')[1])+1),np.arange(int(temp[2].split('-')[0]),int(temp[2].split('-')[1])+1))
                totval = np.append(totval,fields[l[:l.index(': ')]])
            else:
                if l == 'your ticket:\n' or l == 'nearby tickets:\n':
                    pass
                else:
                    tickets.append([int(i) for i in l.split(',')])



        else: 
            fieldsterm = True
unvalid = []
print(len(tickets))
for i in tickets:
    Unvalid = False
    for j in i:
        if j not in totval:
            Unvalid = True
            unvalid.append(j)
    if Unvalid:
        tickets.remove(i)
print(len(tickets))
input()
print(unvalid)
print(np.sum(unvalid))
print(totval.shape)





############################################################## PART 2



fieldpos = {}
for k in fields:
    fieldpos[k] = list(range(len(tickets[0])))

for k in fieldpos:
    for j in tickets:
        for i in range(len(tickets[0])):
            #print(j,len(tickets[0]))
            if j[i] not in fields[k]:
                #print(i," ---> ",j[i]," not in ",fields[k])
                try:
                    fieldpos[k].remove(i)
                    #for l in fieldpos:
                    #    print(fieldpos[l])
                    #input()
                except:
                    print("already removed")
            #else:
            #    print(i," ---> ",j[i]," in ",fields[k])
            #    input()
            
TooLong = True
while TooLong:
    for k in fieldpos:
        if len(fieldpos[k])==1:
            for K in fieldpos:
                if k != K:
                    try:
                        fieldpos[K].remove(fieldpos[k][0])
                    except:
                        i=0
    TooLong = False
    for k in fieldpos:
        if len(fieldpos[k]) != 1 and len(fieldpos[k]) != 0:
            TooLong = True
        print(fieldpos[k])
    #input()

totval = 1
for k in fieldpos:
    if 'departure' in k:
        totval *= tickets[0][fieldpos[k][0]]
print(fieldpos)
print(totval)
