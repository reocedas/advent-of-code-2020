import numpy as np
import itertools
import os
import time
import math


calculus = []

def calculate(line):
    res = 0
    line = line.replace(' ','')
    sub_str = ""
    while '*' in line or '+' in line:
        sym = False
        sub_str = ""
        for i in line:
            if (i == "+" or i == "*") and not sym:
                sym = True
                sub_str = sub_str + i
            elif (i == "+" or i == "*") and sym:
                break
            else : 
                sub_str = sub_str + i

        #print("line : ",line)
        #print("sub_str : ",sub_str)
        #input()
        
        if "(" in sub_str:
            count_par = 0
            new_str = ""
            for i in line[line.index('('):len(line)]:
                if i != "(" and i != ")":
                    new_str = new_str + i
                elif i == ")":
                    count_par -= 1
                    new_str = new_str + i
                    #print(i,count_par)
                elif i == "(":
                    count_par += 1
                    new_str = new_str + i
                    #print(i,count_par)

                if count_par == 0:
                    break
            #print("new_str : ",new_str)
            #print("call function again")
            line = line[0:line.index('(')] + calculate(new_str[1:-1]) + line[line.index(new_str)+len(new_str):]

        elif "+" in sub_str:
            sub_res = int(sub_str[0:sub_str.index("+")]) + int(sub_str[sub_str.index("+")+1:]) 
            line = str(sub_res) + line[len(sub_str):]

        elif "*" in sub_str:
            sub_res = int(sub_str[0:sub_str.index("*")]) * int(sub_str[sub_str.index("*")+1:]) 
            line = str(sub_res) + line[len(sub_str):]
        
                
    return line





with open('entry.dat') as entry:
    for l in entry:
        if l!= '\n':
            calculus.append(l[0:-1])
result = 0
for l in calculus:
    result += int(calculate(l))
    print("line result : ",result)
print("final result : ",result)




####################################################### PART 2

def calculate_v2(line):
    res = 0
    line = line.replace(' ','')
    sub_str = ""
    while '+' in line:
        sym = False
        sub_str = ""
        if "(" not in line:
            for i in line:
                if i == "+":
                    sub_str = "+"
                    j = line.index(i)+1
                    while j>= 0 and j < len(line) and line[j] not in '+*()':
                        sub_str = sub_str+line[j]
                        j+=1
                    j = line.index(i)-1
                    while j>= 0 and j < len(line) and line[j] not in '+*()':
                        sub_str = line[j]+sub_str
                        j-=1
        else:
            sub_str = "("

        #print("line : ",line)
        #print("sub_str : ",sub_str)
        #input()
        
        if "(" in sub_str:
            count_par = 0
            new_str = ""
            for i in line[line.index('('):len(line)]:
                if i != "(" and i != ")":
                    new_str = new_str + i
                elif i == ")":
                    count_par -= 1
                    new_str = new_str + i
                    #print(i,count_par)
                elif i == "(":
                    count_par += 1
                    new_str = new_str + i
                    #print(i,count_par)

                if count_par == 0:
                    break
            #print("new_str : ",new_str)
            #print("call function again")
            line = line[0:line.index('(')] + calculate_v2(new_str[1:-1]) + line[line.index(new_str)+len(new_str):]

        elif "+" in sub_str:
            sub_res = int(sub_str[0:sub_str.index("+")]) + int(sub_str[sub_str.index("+")+1:]) 
            #print("sub res = ",int(sub_str[0:sub_str.index("+")]),"+",int(sub_str[sub_str.index("+")+1:])," = ",sub_res)
            line = line[0:line.index(sub_str)] + str(sub_res) + line[line.index(sub_str)+len(sub_str):]
    #print("+ finished : ",line)
    while '*' in line:
        sym = False
        sub_str = ""
        if "(" not in line:
            for i in line:
                if i == "*":
                    sub_str = "*"
                    j = line.index(i)+1
                    while j>= 0 and j < len(line) and line[j] not in '+*()':
                        sub_str = sub_str+line[j]
                        j+=1
                    j = line.index(i)-1
                    while j>= 0 and j < len(line) and line[j] not in '+*()':
                        sub_str = line[j]+sub_str
                        j-=1
        else:
            sub_str = "("

        #print("line : ",line)
        #print("sub_str : ",sub_str)
        #input()
        
        if "(" in sub_str:
            count_par = 0
            new_str = ""
            for i in line[line.index('('):len(line)]:
                if i != "(" and i != ")":
                    new_str = new_str + i
                elif i == ")":
                    count_par -= 1
                    new_str = new_str + i
                    #print(i,count_par)
                elif i == "(":
                    count_par += 1
                    new_str = new_str + i
                    #print(i,count_par)

                if count_par == 0:
                    break
            #print("new_str : ",new_str)
            #print("call function again")
            line = line[0:line.index('(')] + calculate_v2(new_str[1:-1]) + line[line.index(new_str)+len(new_str):]

        elif "*" in sub_str:
            sub_res = int(sub_str[0:sub_str.index("*")]) * int(sub_str[sub_str.index("*")+1:]) 
            #print("sub_res = ",int(sub_str[0:sub_str.index("*")]) ,"*", int(sub_str[sub_str.index("*")+1:])," = ",sub_res)
            #print("len sub str : ",len(sub_str))
            line = line[0:line.index(sub_str)] + str(sub_res) + line[line.index(sub_str)+len(sub_str):]
 
                
    return line

result = 0
for l in calculus:
    result += int(calculate_v2(l))
    print("line result : ",result)
print("final result : ",result)


