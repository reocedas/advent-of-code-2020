import numpy as np
import os

entry = open("entry.dat","r")

entry = entry.read()

line = []
forest = []
row_num = 0
col_num = 0
ret = False

for i in entry:
    
    if i == '\n':
        row_num += 1
        ret = True
    elif not ret:
        col_num += 1

for i in entry:
    if i == '\n':
        for j in np.arange(row_num):
            for k in np.arange(col_num*10):
                line.append(line[k])
        
        forest.append(line)
        line = []
    else:
        line.append(i)

forest = np.asarray(forest)
print(row_num,col_num)
print(forest.shape)

slope = [1,1]
i = 0
trees1 = 0
while True:
    try:
        a = forest[slope[0]*i,slope[1]*i]
        if a == '#':
            trees1 += 1
            print("tree! %d"%trees1)
        i += 1
    except:
        print("end of the forest! %d trees encountered!"%trees1)
        break

slope = [1,3]
i = 0
trees2 = 0
while True:
    try:
        a = forest[slope[0]*i,slope[1]*i]
        if a == '#':
            trees2 += 1
            print("tree! %d"%trees2)
        i += 1
    except:
        print("end of the forest! %d trees encountered!"%trees2)
        break

slope = [1,5]
i = 0
trees3 = 0
while True:
    try:
        a = forest[slope[0]*i,slope[1]*i]
        if a == '#':
            trees3 += 1
            print("tree! %d"%trees3)
        i += 1
    except:
        print("end of the forest! %d trees encountered!"%trees3)
        break

slope = [1,7]
i = 0
trees4 = 0
while True:
    try:
        a = forest[slope[0]*i,slope[1]*i]
        if a == '#':
            trees4 += 1
            print("tree! %d"%trees4)
        i += 1
    except:
        print("end of the forest! %d trees encountered!"%trees4)
        break

slope = [2,1]
i = 0
trees5 = 0
while True:
    try:
        a = forest[slope[0]*i,slope[1]*i]
        if a == '#':
            trees5 += 1
            print("tree! %d"%trees5)
        i += 1
    except:
        print("end of the forest! %d trees encountered!"%trees5)
        break

print(trees1*trees2*trees3*trees4*trees5)

