import numpy as np
import itertools
import os
import time
import math
import regex

tiles = {}
tiles_ud = {}
tiles_lr = {}
lines = []

num_tile = 0

Tiles = {}



with open('entry.dat') as entry:
    for l in entry:
        if ":" in l:
            num_tile += 1
            tile_ind = int(l[l.index("Tile ")+5:l.index(":")])
            tiles[tile_ind] = []
            Tiles[tile_ind] = np.empty([10,10],dtype = str)

        elif l!="\n":
            lines.append(l[:-1])
        else:
            for i in np.arange(len(lines)):
                for j in np.arange(len(lines[i])):
                    Tiles[tile_ind][i,j] = lines[i][j]
            lines = []


N = {}


######find a corner
for k1 in Tiles:
    #print(k1)
    N[k1] = []
    for op in range(4):
        Tiles[k1] = np.rot90(Tiles[k1])

        for k2 in Tiles:
            if k2 != k1:
                if (Tiles[k1][0,:] == Tiles[k2][-1,:]).all():
                    N[k1].append(k2)
                elif (Tiles[k1][0,:] == np.rot90(Tiles[k2])[-1,:]).all():
                    N[k1].append(k2)
                elif (Tiles[k1][0,:] == np.rot90(np.rot90(Tiles[k2]))[-1,:]).all():
                    N[k1].append(k2)
                elif (Tiles[k1][0,:] == np.rot90(np.rot90(np.rot90(Tiles[k2])))[-1,:]).all():
                    N[k1].append(k2)
                elif (Tiles[k1][0,:] == np.flipud(Tiles[k2])[-1,:]).all():
                    N[k1].append(k2)
                elif (Tiles[k1][0,:] == np.rot90(np.flipud(Tiles[k2]))[-1,:]).all():
                    N[k1].append(k2)
                elif (Tiles[k1][0,:] == np.rot90(np.rot90(np.flipud(Tiles[k2])))[-1,:]).all():
                    N[k1].append(k2)
                elif (Tiles[k1][0,:] == np.rot90(np.rot90(np.rot90(np.flipud(Tiles[k2]))))[-1,:]).all():
                    N[k1].append(k2)
                elif (Tiles[k1][0,:] == np.fliplr(Tiles[k2])[-1,:]).all():
                    N[k1].append(k2)
                elif (Tiles[k1][0,:] == np.rot90(np.fliplr(Tiles[k2]))[-1,:]).all():
                    N[k1].append(k2)
                elif (Tiles[k1][0,:] == np.rot90(np.rot90(np.fliplr(Tiles[k2])))[-1,:]).all():
                    N[k1].append(k2)
                elif (Tiles[k1][0,:] == np.rot90(np.rot90(np.rot90(np.fliplr(Tiles[k2]))))[-1,:]).all():
                    N[k1].append(k2)

count = 1
for k in N:
    if len(N[k]) == 2:
        count*=k
        print(k)
        k1 = k
    elif len(N[k]) > 4:
        print("ALERT")
        

print(count)


################################### PART 2

HW = int(np.sqrt(num_tile))
len_tile = Tiles[k].shape[0]

tot_tile = np.empty((HW*(len_tile-2),HW*(len_tile-2)),dtype = str)
pos_tile = np.empty((HW,HW),dtype =int)
good = 0
while good <2:
    good = 0
    
    for k2 in N[k1]:
                if (Tiles[k1][-1,:] == Tiles[k2][0,:]).all():
                    good += 1
                elif (Tiles[k1][-1,:] == np.rot90(Tiles[k2])[0,:]).all():
                    good += 1
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(Tiles[k2]))[0,:]).all():
                    good += 1
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(np.rot90(Tiles[k2])))[0,:]).all():
                    good += 1
                elif (Tiles[k1][-1,:] == np.flipud(Tiles[k2])[0,:]).all():
                    good += 1
                elif (Tiles[k1][-1,:] == np.rot90(np.flipud(Tiles[k2]))[0,:]).all():
                    good += 1
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(np.flipud(Tiles[k2])))[0,:]).all():
                    good += 1
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(np.rot90(np.flipud(Tiles[k2]))))[0,:]).all():
                    good += 1
                elif (Tiles[k1][-1,:] == np.fliplr(Tiles[k2])[0,:]).all():
                    good += 1
                elif (Tiles[k1][-1,:] == np.rot90(np.fliplr(Tiles[k2]))[0,:]).all():
                    good += 1
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(np.fliplr(Tiles[k2])))[0,:]).all():
                    good += 1
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(np.rot90(np.fliplr(Tiles[k2]))))[0,:]).all():
                    good += 1
                
                
                
                elif (Tiles[k1][:,-1] == Tiles[k2][:,0]).all():
                    good += 1
                elif (Tiles[k1][:,-1] == np.rot90(Tiles[k2])[:,0]).all():
                    good += 1
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(Tiles[k2]))[:,0]).all():
                    good += 1
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(np.rot90(Tiles[k2])))[:,0]).all():
                    good += 1
                elif (Tiles[k1][:,-1] == np.flipud(Tiles[k2])[:,0]).all():
                    good += 1
                elif (Tiles[k1][:,-1] == np.rot90(np.flipud(Tiles[k2]))[:,0]).all():
                    good += 1
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(np.flipud(Tiles[k2])))[:,0]).all():
                    good += 1
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(np.rot90(np.flipud(Tiles[k2]))))[:,0]).all():
                    good += 1
                elif (Tiles[k1][:,-1] == np.fliplr(Tiles[k2])[:,0]).all():
                    good += 1
                elif (Tiles[k1][:,-1] == np.rot90(np.fliplr(Tiles[k2]))[:,0]).all():
                    good += 1
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(np.fliplr(Tiles[k2])))[:,0]).all():
                    good += 1
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(np.rot90(np.fliplr(Tiles[k2]))))[:,0]).all():
                    good += 1
    if good < 2: 
        Tiles[k1] = np.rot90(Tiles[k1])

print("found good orientation")

tot_tile[0:len_tile-2,0:len_tile-2] = Tiles[k1][1:len_tile-1,1:len_tile-1]
pos_tile[0,0] = k1
print(k1)

for i in np.arange(pos_tile.shape[0]):
    for j in np.arange(pos_tile.shape[1]):
        if j != pos_tile.shape[1] -1:
            k1 = pos_tile[i,j]
            for k2 in N[k1]:
                if (Tiles[k1][:,-1] == Tiles[k2][:,0]).all():
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][:,-1] == np.rot90(Tiles[k2])[:,0]).all():
                    Tiles[k2] = np.rot90(Tiles[k2])
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(Tiles[k2]))[:,0]).all():
                    Tiles[k2] = np.rot90(np.rot90(Tiles[k2]))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(np.rot90(Tiles[k2])))[:,0]).all():
                    Tiles[k2] = np.rot90(np.rot90(np.rot90(Tiles[k2])))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][:,-1] == np.flipud(Tiles[k2])[:,0]).all():
                    Tiles[k2] = np.flipud(Tiles[k2])
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][:,-1] == np.rot90(np.flipud(Tiles[k2]))[:,0]).all():
                    Tiles[k2] = np.rot90(np.flipud(Tiles[k2]))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(np.flipud(Tiles[k2])))[:,0]).all():
                    Tiles[k2] = np.rot90(np.rot90(np.flipud(Tiles[k2])))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(np.rot90(np.flipud(Tiles[k2]))))[:,0]).all():
                    Tiles[k2] = np.rot90(np.rot90(np.rot90(np.flipud(Tiles[k2]))))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][:,-1] == np.fliplr(Tiles[k2])[:,0]).all():
                    Tiles[k2] = np.fliplr(Tiles[k2])
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][:,-1] == np.rot90(np.fliplr(Tiles[k2]))[:,0]).all():
                    Tiles[k2] = np.rot90(np.fliplr(Tiles[k2]))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(np.fliplr(Tiles[k2])))[:,0]).all():
                    Tiles[k2] = np.rot90(np.rot90(np.fliplr(Tiles[k2])))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][:,-1] == np.rot90(np.rot90(np.rot90(np.fliplr(Tiles[k2]))))[:,0]).all():
                    Tiles[k2] = np.rot90(np.rot90(np.rot90(np.fliplr(Tiles[k2]))))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
        else:
            k1 = pos_tile[i,0]
            for k2 in N[k1]:
                if (Tiles[k1][-1,:] == Tiles[k2][0,:]).all():
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][-1,:] == np.rot90(Tiles[k2])[0,:]).all():
                    Tiles[k2] = np.rot90(Tiles[k2])
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(Tiles[k2]))[0,:]).all():
                    Tiles[k2] = np.rot90(np.rot90(Tiles[k2]))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(np.rot90(Tiles[k2])))[0,:]).all():
                    Tiles[k2] = np.rot90(np.rot90(np.rot90(Tiles[k2])))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][-1,:] == np.flipud(Tiles[k2])[0,:]).all():
                    Tiles[k2] = np.flipud(Tiles[k2])
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][-1,:] == np.rot90(np.flipud(Tiles[k2]))[0,:]).all():
                    Tiles[k2] = np.rot90(np.flipud(Tiles[k2]))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(np.flipud(Tiles[k2])))[0,:]).all():
                    Tiles[k2] = np.rot90(np.rot90(np.flipud(Tiles[k2])))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(np.rot90(np.flipud(Tiles[k2]))))[0,:]).all():
                    Tiles[k2] = np.rot90(np.rot90(np.rot90(np.flipud(Tiles[k2]))))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][-1,:] == np.fliplr(Tiles[k2])[0,:]).all():
                    Tiles[k2] = np.fliplr(Tiles[k2])
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][-1,:] == np.rot90(np.fliplr(Tiles[k2]))[0,:]).all():
                    Tiles[k2] = np.rot90(np.fliplr(Tiles[k2]))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(np.fliplr(Tiles[k2])))[0,:]).all():
                    Tiles[k2] = np.rot90(np.rot90(np.fliplr(Tiles[k2])))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break
                elif (Tiles[k1][-1,:] == np.rot90(np.rot90(np.rot90(np.fliplr(Tiles[k2]))))[0,:]).all():
                    Tiles[k2] = np.rot90(np.rot90(np.rot90(np.fliplr(Tiles[k2]))))
                    pos_tile[i,j+1] = k2
                    tot_tile[i*len_tile:(1+i)*len_tile,(j+1)*len_tile:(j+2)*len_tile] = Tiles[k2][1:len_tile-1,1:len_tile-1]
                    break


print("complete")






                














