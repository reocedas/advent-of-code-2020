import numpy as np
import os

#entry = ("entry.dat") 

program = {}
i = 0
with open("entry.dat") as entry:
    for l in entry:
        if l != "\n":
            program[i] = l[:-1]
            i+=1
i = 0
acc = 0
exe = []
while True:
    if "acc" in program[i]:
        acc += int(program[i][4:])
        exe.append(i)
        i+=1
    elif "jmp" in program[i]:
        if i + int(program[i][4:]) in exe:
            print("STOP:",acc)
            break
        exe.append(i)
        i = i + int(program[i][4:])
    elif "nop" in program[i]:
        exe.append(i)
        i+=1
buf = []
exit = False
while True:
    for i in program:
        if "nop" in program[i] and i not in buf:
            buf.append(i)
            program[i] = "jmp" + program[i][3:]
            print("line %d replaced"%i, program[i])
            break
        elif "jmp" in program[i] and i not in buf:
            buf.append(i)
            program[i] = "nop" + program[i][3:]
            print("line %d replaced"%i, program[i])
            break
    
    i=0
    acc = 0
    exe = []
    while True:

        if "acc" in program[i]:
            acc += int(program[i][4:])
            exe.append(i)
            if i+1 >= len(program):
                print("SUCCESS:", acc)
                input()    
                exit = True
                break
            i+=1
        elif "jmp" in program[i]:
            if i + int(program[i][4:]) in exe:
                print("STOP:",acc)
                break
            elif i+1 >= len(program):
                print("SUCCESS:", acc)
                input()    
                exit = True
                break
            exe.append(i)
            i = i + int(program[i][4:])
        elif "nop" in program[i]:
            exe.append(i)
            if i+1 >= len(program):
                print("SUCCESS:", acc)
                input()    
                exit = True
                break
            i+=1
    if exit:
        break

    if "nop" in program[buf[-1]]:
        program[buf[-1]] = "jmp" + program[buf[-1]][3:]
        print("line %d restored"%buf[-1],program[buf[-1]])
        
    elif "jmp" in program[buf[-1]]:
        program[buf[-1]] = "nop" + program[buf[-1]][3:]
        print("line %d restored"%buf[-1],program[buf[-1]])

